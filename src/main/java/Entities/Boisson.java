/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Ecode
 */
@Entity

public class Boisson extends Produit implements Serializable {

    @NotNull(message = "The manufacturer name must not be null")
    private float prix;

    public Boisson(Boisson b) {
       super.setNomProduit(b.getNomProduit());
       this.prix = b.getPrix();
    }

    /**
     * @return the prix
     */
    public float getPrix() {
        return round(prix, 2);
    }

    public void setPrix(float prix) {
        this.prix = round(prix, 2);
    }

    public static float round(float d, int decimalPlace) {
        return BigDecimal.valueOf(d).setScale(decimalPlace, BigDecimal.ROUND_HALF_UP).floatValue();
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Boisson)) {
            return false;
        }
        Boisson other = (Boisson) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Boisson[ id=" + id + " ]";
    }

    public Boisson() {
    }

    public Boisson(float prix, String nomProduit) {
        super(nomProduit);
        this.prix = round(prix, 2);
    }

    @Override
    public float calculPrix() {
        return this.getPrix();
    }

}
