/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author francoise
 */
@Embeddable
public class LigneCommandePK implements Serializable {

    @Basic(optional = false)
    @Column(name = "id_produit")
    private Long idProduit;
    @Basic(optional = false)
    @Column(name = "id_commande")
    private Long idCommande;
    @Basic(optional = false)
    @Column(name = "id_taille")
    private Long idTaille;

    public LigneCommandePK() {
    }

    public LigneCommandePK(Long idCommande, Long idProduit, Long idTaille) {
        this.idCommande = idCommande;
        this.idProduit = idProduit;
        this.idTaille = idTaille;
    }

    public LigneCommandePK(Long idCommande, Long idProduit) {
        this.idCommande = idCommande;
        this.idProduit = idProduit;
    }

    public LigneCommandePK(LigneCommandePK lCPK) {
        this.idCommande = lCPK.getIdCommande();
        this.idProduit = lCPK.getIdProduit();
        this.idTaille = lCPK.getIdTaille();
    }

    public Long getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(Long idCommande) {
        this.idCommande = idCommande;
    }

    public Long getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(Long idProduit) {
        this.idProduit = idProduit;
    }

    public Long getIdTaille() {
        return idTaille;
    }

    public void setIdTaille(Long idTaille) {
        this.idTaille = idTaille;
    }
}
