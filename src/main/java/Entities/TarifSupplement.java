/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 *
 * @author Ecode
 */
@Entity
public class TarifSupplement implements Serializable {

    @EmbeddedId
    private TarifSupplementPK tarifSupplementPK;
    private float prixSupplement;

    public TarifSupplement() {
    }

    public TarifSupplement(TarifSupplementPK tarifSupplementPK, float prixSupplement) {
        this.tarifSupplementPK = tarifSupplementPK;
        this.prixSupplement = prixSupplement;
    }

    public TarifSupplementPK getTarifSupplementPK() {
        return tarifSupplementPK;
    }

    public void setTarifSupplementPK(TarifSupplementPK tarifSupplementPK) {
        this.tarifSupplementPK = tarifSupplementPK;
    }

    public float getPrixSupplement() {
        return prixSupplement;
    }

    public void setPrixSupplement(float prixSupplement) {
        this.prixSupplement = prixSupplement;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.getTarifSupplementPK() != null ? this.getTarifSupplementPK().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof TarifSupplement)) {
            return false;
        }
        TarifSupplement other = (TarifSupplement) object;
        if ((this.getTarifSupplementPK() == null && other.getTarifSupplementPK() != null) || (this.getTarifSupplementPK() != null && !this.getTarifSupplementPK().equals(other.getTarifSupplementPK()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.TarifSupplement[ id=" + this.getTarifSupplementPK() + " ]";
    }

}
