/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Ecode
 */
@Entity
@Table
public class Commande implements Serializable {

    @Valid
    @OneToMany(mappedBy="commande", cascade=CascadeType.ALL)
    private List<LigneCommande> ligneCommandes = new ArrayList();
    
    @Basic(optional = false)
    @NotNull
    @ManyToOne()
    @JoinColumn(name="id_user")
    private User user;
    
    @Basic(optional = false)
    @Column(name="date_commande")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCommande ;
    
    private float total;
    
    private boolean statut;
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Commande() {
    }

    public Commande(List<LigneCommande> ligneCommandes, User user) {
        this.ligneCommandes = ligneCommandes;
        this.user = user;
    }

    public List<LigneCommande> getLigneCommandes() {
        return ligneCommandes;
    }

    public void setLigneCommandes(List<LigneCommande> ligneCommandes) {
        this.ligneCommandes = ligneCommandes;
        for(LigneCommande lc : this.ligneCommandes){
            if(lc.getCommande() != this){
                lc.setCommande(this);
            }
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public boolean getStatut() {
        return statut;
    }

    public void setStatut(boolean statut) {
        this.statut = statut;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }   
}
