/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Ecode
 */
@Embeddable

public class TarifPK implements Serializable {

    private Long idTaille;
    private Long idPizza;

    public TarifPK() {
    }

    public TarifPK(Long idTaille, Long idPizza) {
        this.idTaille = idTaille;
        this.idPizza = idPizza;
    }

    public Long getIdTaille() {
        return idTaille;
    }

    public void setIdTaille(Long idTaille) {
        this.idTaille = idTaille;
    }

    public Long getIdPizza() {
        return idPizza;
    }

    public void setIdPizza(Long idTypePizza) {
        this.idPizza = idTypePizza;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idTaille, idPizza);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TarifPK)) {
            return false;
        }
        TarifPK tarifPK = (TarifPK) o;
        return Objects.equals(idTaille, tarifPK.idTaille)
                && Objects.equals(idPizza, tarifPK.idPizza);
    }

    @Override
    public String toString() {
        return "Entities.TarifPK[ id=" + this.getIdTaille() + " " + this.getIdPizza() + " ]";
    }

}
