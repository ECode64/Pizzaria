/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
/**
 *
 * @author Ecode
 */

@Entity
@Table(
    uniqueConstraints= {
        @UniqueConstraint(name="nomTaille_unique", columnNames={"nomTaille"})
    })
public class Taille implements Serializable {
    
    @NotNull
    private String nomTaille;
    
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(cascade=CascadeType.ALL, mappedBy="taille")
    private List<LigneCommande> ligneCommandes;

    public String getNomTaille() {
        return nomTaille;
    }

    public void setNomTaille(String nomTaille) {
        this.nomTaille = nomTaille;
    }

    public Taille(String nomTaille) {
        this.nomTaille = nomTaille;
    }

    public Taille() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Override
    public boolean equals(Object o){
        boolean retour = false;
        if (o instanceof Taille){
            Taille t = (Taille) o;
            if(t.getNomTaille().equals(this.nomTaille)){
                retour = true;
            }
        }
        return retour;
    }
        public Taille(Taille taille) {
        this.id = new Long(taille.getId());
        this.nomTaille = taille.getNomTaille();
    }
}
