/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Ecode
 */
@Entity
@Table
public class LigneCommande implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private LigneCommandePK ligneCommandePK = new LigneCommandePK();
    @Basic(optional = false)
    @NotNull
    @Min(value = 1, message = "Veuillez entrer une quantité.")
    @Column(name = "quantite")
    private int quantite;
    //referencedColumnName nom du champ dans la table cible
    @JoinColumn(name = "id_produit", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Produit produit;
    @JoinColumn(name = "id_commande", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Commande commande = new Commande();
    @JoinColumn(name = "id_taille", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Taille taille = new Taille();
    private float totalLigne;

    public LigneCommande() {
        taille = new Taille();
        produit = new Pizza();
        commande = new Commande();
    }

    public LigneCommande(Long idCommande, Long idTypePizza, Long idTaille) {
        this.ligneCommandePK = new LigneCommandePK(idCommande, idTypePizza, idTaille);
    }
    public LigneCommande(Pizza pizza, Taille taille, int quantite, float totalLigne) {
        this.produit = pizza;
        this.taille = taille;
        this.quantite = quantite;
        this.totalLigne = totalLigne;
    }

    public LigneCommande(Boisson boisson, int quantite, float totalLigne) {
        this.produit = boisson;
        this.quantite = quantite;
        this.totalLigne = totalLigne;
    }

    public LigneCommandePK getLigneCommandePK() {
        return ligneCommandePK;
    }

    public void setLigneCommandePK(LigneCommandePK ligneCommandePK) {
        this.ligneCommandePK = ligneCommandePK;
    }

    public int getQuantite() {
        return quantite;
    }

    public Taille getTaille() {
        return taille;
    }

    public Produit getProduit() {
        return produit;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public float getTotalLigne() {
        return totalLigne;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public void setTypePizza(Pizza typePizza) {
        this.produit = typePizza;
    }

    public void setTaille(Taille taille) {
        this.taille = taille;
    }

    public void setTotalLigne(float totalLigne) {
        this.totalLigne = totalLigne;
    }

    public Commande getCommande() {
        return this.commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }
}
