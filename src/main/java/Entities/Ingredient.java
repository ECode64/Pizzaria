/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Ecode
 */
@Entity
@Table(
        uniqueConstraints = {
            @UniqueConstraint(name = "nom_unique", columnNames = "nomIngredient")})
public class Ingredient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 50)
    @NotNull
    private String nomIngredient;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tarifSupplementPK.idIngredient")
    @OrderBy("id ASC")
    private List<TarifSupplement> tarifSupplements;
    
    public List<TarifSupplement> getTarifSupplements() {
        return tarifSupplements;
    }

    public void setTarifSupplements(List<TarifSupplement> tarifSupplements) {
        this.tarifSupplements = tarifSupplements;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomIngredient() {
        return nomIngredient;
    }

    public void setNomIngredient(String nomIngredient) {
        this.nomIngredient = nomIngredient;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Ingredient)) {
            return false;
        }
        Ingredient other = (Ingredient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Ingredient[ id=" + id + " ]";
    }

    public Ingredient() {
    }

    public Ingredient(String nomIngredient) {
        this.nomIngredient = nomIngredient;
    }

}
