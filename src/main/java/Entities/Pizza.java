/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

/**
 *
 * @author Ecode
 */
@Entity

public class Pizza extends Produit implements Serializable {

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Ingredient> ingredients = new ArrayList();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tarifPK.idPizza")
    @OrderBy("id ASC")
    private List<Tarif> tarifs;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Ingredient> supplements = new ArrayList();

    public List<Tarif> getTarifs() {
        return tarifs;
    }

    public void setTarifs(List<Tarif> tarifs) {
        this.tarifs = tarifs;
    }

    public void setSupplemment(List<Ingredient> supplements) {
        this.supplements = supplements;
    }

    public List<Ingredient> getSupplemment() {
        return supplements;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pizza)) {
            return false;
        }
        Pizza other = (Pizza) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.TypePizza[ id=" + id + " ]";
    }

    public Pizza() {
    }

    public Pizza(String nomProduit) {
        super(nomProduit);
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public float calculPrix() {
        return 0;
    }

    public void ajoutIngredient(Ingredient i) {
        ingredients.add(i);
    }

    public Pizza(Pizza typePizza) {
        this.setId(typePizza.getId());
        super.setNomProduit(typePizza.getNomProduit());
    }

}
