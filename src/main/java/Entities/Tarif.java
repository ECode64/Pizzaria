/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Ecode
 */
@Entity
public class Tarif implements Serializable {

   @EmbeddedId
    private TarifPK tarifPK;

    private float prix;

    public Tarif() {
    }

    public Tarif(TarifPK tarifPK, float prix) {
        this.tarifPK = tarifPK;
        this.prix = prix;
    }

    public TarifPK getTarifPK() {
        return tarifPK;
    }

    public void setTarifPK(TarifPK tarifPK) {
        this.tarifPK = tarifPK;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix)
    {
        this.prix = prix;
    }

    @Override
    public int hashCode() {
        return Objects.hash(tarifPK, prix);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tarif)) return false;
        Tarif tarif = (Tarif) o;
        return Float.compare(tarif.prix, prix) == 0 &&
                Objects.equals(tarifPK, tarif.tarifPK);
    }

    @Override
    public String toString() {
        return "Entities.Tarif[ id=" + this.getTarifPK() + " ]";
    }

}
