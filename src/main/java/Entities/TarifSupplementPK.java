/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 *
 * @author Ecode
 */
@Embeddable
public class TarifSupplementPK implements Serializable {

    private Long idTaille;
    private Long idIngredient;

    public TarifSupplementPK() {
    }

    public TarifSupplementPK(Long idTaille, Long idIngredient) {
        this.idTaille = idTaille;
        this.idIngredient = idIngredient;
    }

    public Long getIdTaille() {
        return idTaille;
    }

    public void setIdTaille(Long idTaille) {
        this.idTaille = idTaille;
    }

    public Long getIdIngredient() {
        return idIngredient;
    }

    public void setIdIngredient(Long idIngredient) {
        this.idIngredient = idIngredient;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTaille != null ? idTaille.hashCode() : 0);
        hash += (idIngredient != null ? idIngredient.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof TarifSupplementPK)) {
            return false;
        }
        TarifSupplementPK other = (TarifSupplementPK) object;
        if ((this.idTaille == null && other.idTaille != null) || (this.idTaille != null && !this.idTaille.equals(other.idTaille))) {
            return false;
        }
        if ((this.idIngredient == null && other.idIngredient != null) || (this.idIngredient != null && !this.idIngredient.equals(other.idIngredient))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.TarifSupplementPK[ id=" + idTaille + " " + idIngredient + " ]";
    }

}
