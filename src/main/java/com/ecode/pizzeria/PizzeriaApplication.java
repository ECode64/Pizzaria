/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecode.pizzeria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author Ecode
 */
//Active les fichiers xml de configuration

//active la dispatcherServlet

    /**
     *
     */

@SpringBootApplication
@ComponentScan("Control")

public class PizzeriaApplication {

    public static void main(String[] args) {
            ApplicationContext ctx = SpringApplication.run(PizzeriaApplication.class, args);
        }
}

