/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import security.CustomUserDetailsService;

/**
 *
 * @author Ecode
 */

@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = CustomUserDetailsService.class)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Autowired 
    private UserDetailsService userDetailsService;
 
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //uniquement pour test junit à décommenter 
        http.csrf().disable();
        http
            .authorizeRequests()
                .antMatchers("/user/**").hasRole("USER")
                .antMatchers("/admin/*").access("hasRole('ROLE_ADMIN')")
                .antMatchers("/employee/**").hasRole("EMPLOYEE")
                .antMatchers("/", "favicon.ico", "/**","/images/**","/webjars/**")
                    .permitAll()
                        .anyRequest()
                            .authenticated()
            .and()
                .formLogin()
                    .loginPage("/login")
                        .permitAll()
            .and()
                .logout()
                    .logoutUrl("/logout")
                        .permitAll();
    }
    
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .inMemoryAuthentication()
                .withUser("user").password("password").roles("USER");
        auth
            .inMemoryAuthentication()
            .withUser("admin").password("password").roles("ADMIN", "EMPLOYEE", "USER");
        auth
            .inMemoryAuthentication()
            .withUser("employee").password("password").roles("EMPLOYEE", "USER");
    }
    
    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {    
	auth.userDetailsService(userDetailsService).passwordEncoder(passwordencoder());
    } 
    
    @Bean(name="passwordEncoder")
    public PasswordEncoder passwordencoder(){
        return new BCryptPasswordEncoder();
    }
}
