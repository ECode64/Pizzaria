package Control;

import Entities.Commande;
import Entities.LigneCommande;
import Entities.Pizza;
import Entities.Taille;
import Entities.TarifPK;
import Entities.User;
import Repositories.CommandeRepository;
import Repositories.LigneCommandeRepository;
import Repositories.PizzaRepository;
import Repositories.TailleRepository;
import Repositories.TarifRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.Collection;
import java.util.Date;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Ecode
 */
@Controller
@ComponentScan({"Entities", "Repositories"})
public class CommandeController {

    @Autowired
    private CommandeRepository commandeRepository;
    @Autowired
    private LigneCommandeRepository ligneCommandeRepository;

    private Collection<LigneCommande> lignesCommande = new ArrayList();

    @Autowired
    private PizzaRepository pizzaRepository;

    @Autowired
    private TailleRepository tailleRepository;

    @Autowired
    private TarifRepository tarifRepository;

    @GetMapping("/commander")
    public String commander() {
        return ("commander");
    }

    @GetMapping("/commanderB")
    public String commanderB() {
        return ("commanderB");
    }

    @GetMapping("/commandeValidee")
    public String commandeValidee() {
        return ("commandeValidee");
    }

    @ModelAttribute("commande")
    public Commande creerCommande() {
        return new Commande();
    }

    /*
    EnVOIE LIGNE COMMANDE
     */
    @PostMapping("/validerCommande")
    public ModelAndView validerCommande(@AuthenticationPrincipal User activeUser) {
        Commande commande = new Commande(); //créer commande
        commande.setUser(activeUser);
        commande.setDateCommande(new Date());
        commande.setStatut(false);
        Long idCom = commandeRepository.save(commande).getId(); //save et récupérer id de la commande 
        Commande commande2 = new Commande();//recuperer derniere commande 
        commande2.setId(idCom);

        for (LigneCommande lC : lignesCommande) {
            lC.setCommande(commande2);
            lC.getLigneCommandePK().setIdCommande(idCom);
            lC.getLigneCommandePK().setIdTaille(lC.getTaille().getId());
            lC.getLigneCommandePK().setIdProduit(lC.getProduit().getId());
            lC.getCommande().setId(idCom);
            commande.setTotal(lC.getTotalLigne());
            System.out.println("Id Commande : " + lC.getCommande().getId());
            System.out.println("Id Commande : " + lC.getProduit().getNomProduit());
            System.out.println("Id Commande : " + lC.getTaille().getNomTaille());
        }

        ligneCommandeRepository.save(lignesCommande);
        lignesCommande.clear();
        return new ModelAndView("/commandeValidee", "idCommande", "Votre numéro de commande est le " + String.valueOf(idCom));
    }

    @GetMapping("/mesCommandes")
    public String mesCommandes(@AuthenticationPrincipal User activeUser, Model model) {
        model.addAttribute("allCommandes", this.commandeRepository.findByUserOrderByDateCommandeDesc(activeUser));
        return "mesCommandes";
    }

    /*
    CREER LIGNE COMMANDE
     */
    @PostMapping("/commander")
    public ModelAndView commander(@ModelAttribute("ligneCommande") LigneCommande lC) {
        Pizza tp = new Pizza(pizzaRepository.findOne(lC.getProduit().getId()));
        Taille t = new Taille(tailleRepository.findOne(lC.getTaille().getId()));
        if (lC.getQuantite() == 0) {
            return new ModelAndView("commander", "message", "Quantite : " + lC.getQuantite() + " selectionez une quantite");
        }
        int quantite = lC.getQuantite();
        TarifPK tPk = new TarifPK(t.getId(), tp.getId());
        float totalLigne = tarifRepository.findByTarifPK(tPk).getPrix() * quantite;

        LigneCommande ligneTemp = new LigneCommande(tp, t, quantite, totalLigne);
        lignesCommande.add(ligneTemp);
        return new ModelAndView("redirect:/panier.htm");

    }

    /*
        AFFICHER PANIER
     */
    @RequestMapping(value = "/voirPanier", method = RequestMethod.POST)
    public String voirPanier() {
        return "panier";
    }

    /*
    CALCULER TOTAL
     */
    public float calculerTotalCommande(Collection<LigneCommande> LsC) {
        float total = 0.0f;
        for (LigneCommande lC : LsC) {
            total = total + lC.getTotalLigne();
        }
        return total;
    }

    @ModelAttribute("total")
    public String afficherTotal() {
        return String.valueOf(calculerTotalCommande(lignesCommande));
    }

    @ModelAttribute("panier")
    public Collection<LigneCommande> affichePanier() {
        return lignesCommande;
    }

    @GetMapping("/panier")
    public String getPanier() {
        return ("panier");
    }

    @GetMapping("detailCommande/{id}")
    public String detailCommande(@PathVariable Long id, Model model) {
        model.addAttribute("commande", commandeRepository.findOne(id));
        return "detailCommande";
    }

}
