/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import Entities.User;
import Entities.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import Repositories.UserRepository;
import org.springframework.web.servlet.ModelAndView;
import Repositories.UserRolesRepository;

/**
 *
 * @author Ecode
 */
@Controller
@EntityScan("Entities")
@EnableJpaRepositories("Repositories")

public class LoginController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserRolesRepository userRolesRepository;

    @ModelAttribute("user")
    public User newUser() {

        return new User();
    }

    @GetMapping("/login")
    public String loginGet() {
        return ("login");
    }

    @PostMapping("/login")
    public String loginPost() {
        return ("index");
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "redirect:/";
    }

    @GetMapping("/inscription")
    public String inscriptionGet() {
        return ("inscription");
    }

    @PostMapping("/inscription")
    public ModelAndView inscriptionPost(@ModelAttribute("user") @Valid User user, BindingResult result) {
        /*
    BindingResult interface qui permet d'appliquer un validateur et de lier les résultats avec les vues
         */
        if (result.hasErrors()) {
            return new ModelAndView("inscription");
        }
        user.setEnabled(1);
        String password = user.getPassword();
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        user.setPassword(hashedPassword);
        try {
            userRepository.save(user);
            UserRole userRole = new UserRole();
            userRole.setUserid(user.getUserid());
            userRole.setRole("ROLE_USER");
            userRolesRepository.save(userRole);
        } catch (Exception error) {
            return new ModelAndView("login", "message", user.getUserName().toLowerCase() + " existe déjà!. Connectez-vous.");
        }

        return new ModelAndView("login", "message", user.getUserName().toLowerCase() + " ajouté! Connectez-vous.");

    }

}
