/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Entities.Boisson;
import Repositories.BoissonRepository;
import java.util.ArrayList;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Ecode
 */
@Controller
@EntityScan("Entities")
@EnableJpaRepositories("Repositories")


public class BoissonController {

    @Autowired
    BoissonRepository boissonRepository;

    ArrayList<String> messages = new ArrayList<>();

    @GetMapping("/ajoutBoisson")
    public String ajoutBoissonGet() {
        return ("ajoutBoisson");
    }

    @PostMapping("/ajoutBoisson")
    public ModelAndView ajoutBoissonPost(@ModelAttribute("boisson") @Valid Boisson boisson, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ModelAndView("ajoutBoisson");
        }
        try {
            boissonRepository.save(boisson);
        } catch (Exception error) {
            return new ModelAndView("ajoutBoisson", "message", boisson.getNomProduit() + " existe déjà");
        }

        return new ModelAndView("redirect:/ajoutBoisson.htm", "message", boisson.getNomProduit() + " ajouté");
    }
}
