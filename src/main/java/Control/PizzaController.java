/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Entities.*;
import Repositories.*;
import groovyjarjarcommonscli.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Ecode
 */
@Controller
@ComponentScan({"Entities", "Repositories"})
public class PizzaController {

    @Autowired
    PizzaRepository pizzaRepository;
    @Autowired
    IngredientRepository ingredientRepository;
    @Autowired
    TailleRepository tailleRepository;
    @Autowired
    TarifRepository tarifRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    LigneCommandeRepository ligneCommandeRepository;
    @Autowired
    CommandeRepository commandeRepository;

    protected final Log logger = LogFactory.getLog(getClass());

    @GetMapping("/")
    public String index() {
        return ("index");
    }

    @GetMapping("/ajoutPizza")
    public String ajoutPizzaGet() {
        return ("ajoutPizza");

    }

    @RequestMapping(value = "/ajoutPizza", method = RequestMethod.POST)
    public ModelAndView ajoutPizza(@ModelAttribute("pizza") @Valid Pizza pizza, RedirectAttributes model, BindingResult bindingResult, WebRequest webRequest) throws ParseException {
        if (bindingResult.hasErrors()) {
            return new ModelAndView("ajoutPizza");
        }
        List<Ingredient> selectedIngredient = new ArrayList<>();
        Map<String, String[]> map = webRequest.getParameterMap();
        Iterator iterator = webRequest.getParameterNames();
        List<Ingredient> ingredients = new ArrayList<>();
        map.keySet().forEach((keys) -> {
            String[] value = map.get(keys);
            for (String str : value) {
                String keyValue = iterator.next().toString();
                try {
                    if (str.equals("on")) {
                        selectedIngredient.add(ingredientRepository.findByNomIngredient(keyValue)
                        );
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        try {
            model.addAttribute("ingredients", ingredients);
            if (selectedIngredient.size() < 1) {
                return new ModelAndView("ajoutPizza", "message", pizza.getNomProduit().toLowerCase() + " doit contenir au moins un ingredient");
            }
            pizza.setIngredients(selectedIngredient);
            pizza.setNomProduit(pizza.getNomProduit());

            pizzaRepository.save(pizza);

            return new ModelAndView("redirect:/ajoutPizza.htm", "message", pizza.getNomProduit().toLowerCase() + " a été rajoutée!" + "N° de Ingredient(s) : " + pizza.getIngredients().size());

        } catch (Exception e) {
            return new ModelAndView("ajoutPizza", "message", pizza.getNomProduit().toLowerCase() + " existe déjà!");
        }
    }

    @GetMapping("/listePizzas")
    public String listePizzas() {
        return ("listePizzas");
    }

}
