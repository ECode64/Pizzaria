/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Entities.Tarif;
import Entities.TarifSupplement;
import Repositories.TarifRepository;
import Repositories.TarifSupplementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import javax.validation.Valid;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author Ecode
 */
@Controller
@ComponentScan({"Entities"})
@EnableJpaRepositories("Repositories")
public class TarifController {

    @Autowired
    private TarifRepository tarifRepository;
    @Autowired
    private TarifSupplementRepository tarifSupplementRepository;

    @GetMapping("/ajoutTarif")
    public String tarifPizza() {
        return ("ajoutTarif");
    }

    @PostMapping("/ajoutTarif")
    public String saveTarif(@ModelAttribute("tarif") @Valid Tarif tarif, BindingResult result) {
        if (result.hasErrors()) {
            return ("ajoutTarif");
        }
        tarifRepository.save(tarif);
        return ("redirect:/ajoutTarif.htm");
    }

    @GetMapping("/ajoutTarifSup")
    public String tarifIng() {
        return ("ajoutTarifSup");
    }

    @PostMapping("/ajoutTarifSup")
    public String saveTarifSup(@ModelAttribute("tarifSupplement") @Valid TarifSupplement tarif, BindingResult result) {
        if (result.hasErrors()) {
            return ("ajoutTarifSup");
        }
        tarifSupplementRepository.save(tarif);
        return ("redirect:/ajoutTarifSup.htm");
    }

}
