/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import javax.validation.Valid;
import Entities.*;
import Repositories.TailleRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Ecode
 */

@Controller
@ComponentScan("Entities")
public class TailleController {

    @Autowired
    private TailleRepository tailleRepository;


    @ModelAttribute("taille")
    public Taille newIngredient() {
        return new Taille();
    }


    @GetMapping("/ajoutTaille")
    public String ajoutTailleGet() {
        return ("ajoutTaille");
    }
    @GetMapping("/taiiles")
    public List<Taille> listeTailles() {
        return tailleRepository.findAll();

    }
    
    @RequestMapping(value = "/ajoutTaille", method = RequestMethod.POST)
    public ModelAndView ajoutTaillePost(@ModelAttribute("taille") @Valid Taille taille, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ModelAndView("ajoutTaille");
        }
        try {
            tailleRepository.save(taille);

        } catch (Exception error) {
            return new ModelAndView("ajoutTaille", "message", taille.getNomTaille()+ " existe déjà");
        }

        return new ModelAndView("redirect:/ajoutTaille.htm", "message", taille.getNomTaille()+ " ajouté");
    }
}
