/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Entities.*;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import Repositories.*;
import java.util.ArrayList;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;

/**
 *
 * @author Ecode
 */
@ControllerAdvice
public class SuperController {

    @Autowired
    private BoissonRepository boissonRepository;

    @Autowired
    private IngredientRepository ingredientRepository;
    @Autowired
    private TailleRepository tailleRepository;
    @Autowired
    private PizzaRepository pizzaRepository;
    @Autowired
    private TarifRepository tarifRepository;
    @Autowired
    private TarifSupplementRepository tarifSupplementRepository;
    @Autowired
    private LigneCommandeRepository ligneCommandeRepository;

    @ModelAttribute("boisson")
    public Boisson newBoisson() {
        return new Boisson();
    }

    @ModelAttribute("boissons")
    Collection<Boisson> findAllBoisson() {
        Collection<Boisson> boissons = (Collection<Boisson>) boissonRepository.findAll();
        return boissons;
    }

    //MODEL ATTRIBUTES
    @ModelAttribute("pizza")
    public Pizza newTypePizza() {
        return new Pizza();
    }

    @ModelAttribute("pizzas")
    ArrayList<Pizza> findAllPizzas() {
        ArrayList<Pizza> pizzas = (ArrayList<Pizza>) pizzaRepository.findAll();
        return pizzas;
    }

    @ModelAttribute("ingredient")
    public Ingredient newIngredient() {
        return new Ingredient();
    }

    @ModelAttribute("ingredients")
    Collection<Ingredient> findAllIngredients() {
        Collection<Ingredient> ingredients = (Collection<Ingredient>) ingredientRepository.findAll();
        return ingredients;
    }

    @ModelAttribute("taille")
    public Taille newTaille() {
        return new Taille();
    }

    @ModelAttribute("tailles")
    Collection<Taille> findAllTailles() {
        Collection<Taille> tailles = (Collection<Taille>) tailleRepository.findAll();
        return tailles;
    }

    @ModelAttribute("tarifPK")
    public TarifPK newTarifPK() {
        return new TarifPK();
    }

    @ModelAttribute("tarif")
    public Tarif newTarif() {
        return new Tarif();
    }

    @ModelAttribute("tarifs")
    ArrayList<Tarif> findAllTarifs() {
        ArrayList<Tarif> tarifs = (ArrayList<Tarif>) tarifRepository.findAll();
        return tarifs;
    }

    @ModelAttribute("tarifSupplementPK")
    public TarifSupplementPK newTarifSupplementPK() {
        return new TarifSupplementPK();
    }

    //Tarif
    @ModelAttribute("tarifSupplement")
    public TarifSupplement newTarifSupplement() {
        return new TarifSupplement();
    }

    @ModelAttribute("tarifSupplements")
    ArrayList<TarifSupplement> findAllTarifSupplements() {
        ArrayList<TarifSupplement> tarifs = (ArrayList<TarifSupplement>) tarifSupplementRepository.findAll();
        return tarifs;
    }

    @ModelAttribute("user")
    public User newUser() {
        return new User();
    }

    @ModelAttribute("ligneCommandePK")
    public LigneCommandePK newLigneCommandePK() {
        return new LigneCommandePK();
    }

    @ModelAttribute("ligneCommande")
    public LigneCommande newLigneCommande() {

        return new LigneCommande();
    }

    @ModelAttribute("lignesCommande")
    public Collection<LigneCommande> findAllLigneCommande() {
        Collection<LigneCommande> lignesCom = (Collection<LigneCommande>) ligneCommandeRepository.findAll();
        return lignesCom;
    }

    @ModelAttribute("currentUser")
    User currentUser(@AuthenticationPrincipal User activeUser) {
        if (activeUser != null) {
            return activeUser;
        }
        return null;
    }

    @ModelAttribute("currentAuth")
    Authentication currentAuth(Authentication authentication) {
        if (authentication != null) {
            return authentication;
        }
        return null;
    }
}
