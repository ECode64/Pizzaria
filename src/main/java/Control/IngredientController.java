package Control;

import Entities.Ingredient;
import Repositories.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Ecode
 */
@Controller
@ComponentScan({"Entities"})
public class IngredientController {

    @Autowired
    private IngredientRepository ingredientRepository;

    @GetMapping("/ajoutIngredient")
    public String formIngredients() {
        return ("ajoutIngredient");
    }

    @PostMapping("/ajoutIngredient")
    public ModelAndView saveIngredient(@ModelAttribute("ingredient") @Valid Ingredient ingredient, BindingResult result) {
        if (result.hasErrors()) {
           return new ModelAndView("ajoutIngredient");
        }
        try {
            ingredientRepository.save(ingredient);
        } catch (Exception e) {
             return new ModelAndView("ajoutIngredient", "message", ingredient.getNomIngredient() + " existe déjà!.");
        }
        
       return new ModelAndView("redirect:/ajoutIngredient.htm", "message", ingredient.getNomIngredient() + " Ajouté!.");
    }
    
}
