/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repositories;

/**
 *
 * @author Ecode
 */
    
import Entities.TarifSupplement;
import Entities.TarifSupplementPK;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TarifSupplementRepository extends CrudRepository<TarifSupplement, Long> {

    TarifSupplement findByTarifSupplementPK(TarifSupplementPK tarifSupplementPK);

    List<TarifSupplement> findByPrixSupplementGreaterThan(float prixSupplement);

    @Override
    List<TarifSupplement> findAll();

}
