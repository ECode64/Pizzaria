/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repositories;

import java.util.List;
import Entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ecode
 */

@Repository
public interface UserRepository  extends CrudRepository<User, Long> {
    public User findByUserName(String username);
    
    @Query("SELECT u FROM User u, UserRole r where u.id = r.userid and r.role = :role")
    public List<User> findEmployees(@Param("role") String role);
}
