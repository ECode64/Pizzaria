/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repositories;

import Entities.Taille;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ecode
 */
@Repository
public interface TailleRepository extends CrudRepository<Taille, Long> {

        @Override
    Taille save(Taille taille);

    @Override
    List<Taille> findAll();

    Taille findByNomTaille(String nomTaille);
    
}
