/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repositories;

import Entities.Boisson;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ecode
 */
@Repository
public interface BoissonRepository extends CrudRepository<Boisson, Long> {
    @Override
    Boisson save(Boisson boisson);

    @Override
    List<Boisson> findAll();

    Boisson findByNomProduit(String nameBoisson);
}
    