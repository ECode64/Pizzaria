/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repositories;

import java.util.List;
import Entities.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ecode
 */
@Repository
public interface TarifRepository extends CrudRepository<Tarif, Long> {

    public Tarif findByTarifPK(TarifPK tarifPK);

    public List<Tarif> findByPrixGreaterThan(float prix);

    @Override
    Tarif save(Tarif tarif);
}
