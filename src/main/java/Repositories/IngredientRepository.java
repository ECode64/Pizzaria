/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repositories;

import Entities.Ingredient;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Ecode
 */

@Repository
public interface IngredientRepository extends CrudRepository<Ingredient, Long> {
    
    @Override
    Ingredient save(Ingredient ingredient);

    @Override
    List<Ingredient> findAll();

    Ingredient findByNomIngredient(String nameIngredient);

}
